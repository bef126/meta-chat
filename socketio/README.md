# MetaChat - Socket.io

To run the server, first install the dependancies by running `npm install` in the socketio directory. Then you can start the server by running `node server.js`

To run the client, open a web browser to `http://localhost:3000/[room_name]` where `[room_name]` is the chatroom you want to join. For example, to join the `foo` room, you would visit `http://localhost:3000/foo`. When the page loads you will be prompted for your name. Once you enter your name, you may begin chatting.