var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.get('/:room', function (req, res) {
  res.sendfile('client.html');
})

app.get('/', function (req, res) {
  res.send('Welcome to meta-chat server. Join a room by going to the url /roomname');
})

io.on('connection', function(socket){
  console.log('a user connected');
  socket.on('disconnect', function(){
    console.log('user disconnected');
  });
  
  // join a socket connection to a channel
  socket.on('join_channel', function(channel){
    socket.channel = channel;
    socket.join(channel);
  });
  
  // when we receive a chat msg, broadcast it to everyone in the channel
  socket.on('chat', function(channel, data){
    io.sockets.in(channel).emit('chat', data);
  });
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});