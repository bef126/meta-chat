# MetaChat

This repository contains my solution to the web chat problem. I submitted 2 solutions. If I was asked to build a chat server in production, I would likely use socket.io. socket.io is very lightweight and scales well for this sort of application (I've read about people running 5,000 concurrent socket.io connections on a single EC2 instance).

Since the position I'm applying for requires Go on AppEngine, I also decided to submit an AppEngine version written in Go to demonstrate my understanding of the technology. I used the channels API. The AppEngine channels are like socket.io connections in that they implement the websockets protocol, however there are some key differences:

1. They are uni-directional. You can emit messages from the server to the client, but not the other way. For this reason, the client has to POST the chat message to the server.
2. You can only have 1 client/connection per channel. So unlike socket.io where you can use a pub/sub model by having various clients listening to the same channel and let socket.io do all the multiplexing, in AppEngine you have to run your own loop and emit to each channel discretely.