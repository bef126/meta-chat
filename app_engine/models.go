package metachat

import (
	"appengine"
	"appengine/channel"
	"appengine/datastore"
	"appengine/memcache"
	"errors"
)

// a chat room. each room can have multiple clients
type Room struct {
	Name string
}

// we use the room name as the key. each room's key is used as the
// ancestor so that every client in the same room belongs to the same
// entity group
func (r *Room) Key(c appengine.Context) *datastore.Key {
	return datastore.NewKey(c, "Room", r.Name, 0, nil)
}

// return a list of all clients in this room
func (r *Room) Clients(c appengine.Context) ([]Client, error) {
	var clients []Client

	// attempt to pull from memcache
	_, err := memcache.JSON.Get(c, r.Name, &clients)
	if err != nil && err != memcache.ErrCacheMiss {
		return clients, err
	}

	// if we get a cache miss, read from datastore then write back into cache
	if err == memcache.ErrCacheMiss {
		q := datastore.NewQuery("Client").Ancestor(r.Key(c))
		_, err = q.GetAll(c, &clients)
		if err != nil {
			return clients, err
		}
		err = memcache.JSON.Set(c, &memcache.Item{
			Key: r.Name, Object: clients,
		})
		if err != nil {
			return clients, err
		}
	}
	return clients, nil
}

// determine if a client with the same name already exists in this room
func (r *Room) ClientExists(c appengine.Context, alias string) (bool, error) {
	clients, err := r.Clients(c)
	if err != nil {
		return false, err
	}
	for _, client := range clients {
		if client.Alias == alias {
			return true, nil
		}
	}
	return false, nil
}

// add a client to a room. once added, the client will receive all channel events
// we use an ancestor query to keep all clients in the same entity group as the room
func (r *Room) AddClient(c appengine.Context, alias string) (string, error) {
	// do not allow 2 users with the same name to join the same room
	b, err := r.ClientExists(c, alias)
	if err != nil {
		return "", err
	}
	if b == true {
		return "", errors.New("Name taken")
	}

	key := datastore.NewKey(c, "Client", alias, 0, r.Key(c))
	client := &Client{alias}
	_, err = datastore.Put(c, key, client)
	if err != nil {
		return "", err
	}

	// Purge the now-invalid cache record (if it exists).
	memcache.Delete(c, r.Name)

	return channel.Create(c, alias)
}

// remove a client from the room, if it exists
func (r *Room) RemoveClient(c appengine.Context, alias string) error {
	b, err := r.ClientExists(c, alias)
	if err != nil {
		return err
	}
	if b == true {
		key := datastore.NewKey(c, "Client", alias, 0, r.Key(c))
		err := datastore.Delete(c, key)
		if err != nil {
			return err
		}
		// Purge the now-invalid cache record (if it exists).
		memcache.Delete(c, r.Name)
	}
	return nil
}

// broadcast a message to everybody in the room
func (r *Room) Send(c appengine.Context, message, from string) error {
	clients, err := r.Clients(c)
	if err != nil {
		return err
	}

	payload := ChatPayload{message, from, "chat"}
	for _, client := range clients {
		err = channel.SendJSON(c, client.Alias, payload)
		if err != nil {
			c.Errorf("error sending %q: %v", message, err)
		}
	}

	return nil
}

// send a private message to a user
func (r *Room) SendPM(c appengine.Context, message, from, to string) error {
	var clients []Client
	q := datastore.NewQuery("Client").Ancestor(r.Key(c)).Filter("Alias =", to).Limit(1)
	_, err := q.GetAll(c, &clients)
	if err != nil {
		return err
	}

	if len(clients) == 1 {
		payload := ChatPayload{message, from, "pm"}
		err = channel.SendJSON(c, clients[0].Alias, payload)
		if err != nil {
			return err
		}
	} else {
		return errors.New("client not found")
	}

	return nil
}

// getRoom fetches a Room by name from the datastore,
// creating it if it doesn't exist already.
func getRoom(c appengine.Context, name string) (*Room, error) {
	room := &Room{name}

	fn := func(c appengine.Context) error {
		err := datastore.Get(c, room.Key(c), room)
		if err == datastore.ErrNoSuchEntity {
			_, err = datastore.Put(c, room.Key(c), room)
		}
		return err
	}

	return room, datastore.RunInTransaction(c, fn, nil)
}

// return a list of all the rooms in the datastore
func rooms(c appengine.Context) ([]Room, error) {
	var rooms []Room
	q := datastore.NewQuery("Room")
	_, err := q.GetAll(c, &rooms)
	if err != nil {
		return rooms, err
	}
	return rooms, nil
}

// Client is a participant in a chat Room.
type Client struct {
	Alias string
}

// the object that broadcast to everyone in the chatroom
type ChatPayload struct {
	Msg  string
	User string
	Type string // the type of chat msg, either 'chat' or 'pm'
}
