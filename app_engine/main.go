package metachat

import (
	"appengine"
	"fmt"
	"html/template"
	"net/http"
	"regexp"
)

func init() {
	http.HandleFunc("/", root)
	http.HandleFunc("/join", join)
	http.HandleFunc("/post", post)
	http.HandleFunc("/pm", pm)
	http.HandleFunc("/_ah/channel/disconnected/", disconnect)
}

// join a client to a room. return token as JSON document
func join(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)

	// Get or create the Room.
	room, err := getRoom(c, r.FormValue("room"))
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	// Create a new Client, getting the channel token.
	token, err := room.AddClient(c, r.FormValue("name"))
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	var m = map[string]string{
		"token": token,
	}

	w.Header().Set("Content-Type", "application/json")
	fmt.Fprint(w, mapToJson(m))
}

// render the chat html page
func root(w http.ResponseWriter, r *http.Request) {
	// Get the room name from the request URL.
	name := r.URL.Path[1:]
	// validate the room name contains only alpha-numerics
	var validName = regexp.MustCompile(`^[a-zA-Z0-9\-]+$`)
	if !validName.MatchString(name) {
		http.Error(w, "Invalid name", 404)
		return
	}

	c := appengine.NewContext(r)

	// Get or create the Room.
	room, err := getRoom(c, name)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	// Render the HTML template.
	type TemplData struct {
		Room string
	}
	data := TemplData{room.Name}
	raw, err := readTemplate("index.html")
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	t, err := template.New("root").Parse(raw)
	err = t.Execute(w, data)
	if err != nil {
		http.Error(w, err.Error(), 500)
	}
}

// post broadcasts a message to a specified Room.
func post(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)

	// Get the room.
	room, err := getRoom(c, r.FormValue("room"))
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	// Send the message to the clients in the room.
	err = room.Send(c, r.FormValue("msg"), r.FormValue("name"))
	if err != nil {
		http.Error(w, err.Error(), 500)
	}
}

// remove the client record once the client channel disconnects
func disconnect(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)

	alias := r.FormValue("from")

	rooms, err := rooms(c)
	if err != nil {
		http.Error(w, err.Error(), 500)
	}
	for _, room := range rooms {
		room.RemoveClient(c, alias)
	}
}

// send a PM from one client to another
func pm(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)

	// Get or create the Room.
	room, err := getRoom(c, r.FormValue("room"))
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	err = room.SendPM(c, r.FormValue("msg"), r.FormValue("from"), r.FormValue("to"))
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	fmt.Fprint(w, "OK")
}
