# MetaChat - AppEngine

To deploy this app, simply copy `app.yaml.sample` to `app.yaml` and fill in the app identifier. Then deploy to AppEngine `appcfg.py update .`

To connect to a chatroom, visit the URL and add a `/[room_name]`. For example, if you deployed the app to `http://myapp.appspot.com` and you wanted to join the `foo` chatroom, simply visit `http://myapp.appspot.com/foo`. When you first load a room in your browser, you will be prompted to enter your name. If you have chosen a name that is already in use in that room, you will be prompted for another name. Once you have a name, you may begin chatting by typing your message in the input at the bottom of the screen and hitting enter.

To run the test suite, simply run `goapp test` in the `app_engine` directory