package metachat

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

// convert a map to a json-formatted string
func mapToJson(m map[string]string) string {
	b, err := json.Marshal(m)
	if err != nil {
		fmt.Println("error:", err)
		return ""
	}
	return string(b[:])
}

// read a template from the views directory into a string
func readTemplate(filename string) (string, error) {
	ba, err := ioutil.ReadFile("views/" + filename)
	if err != nil {
		return "", err
	}

	// convert the byte array to a string
	raw := string(ba)

	return raw, nil
}
