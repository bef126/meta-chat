package metachat

import (
	"appengine/aetest"
	"appengine/datastore"
	"testing"
	"time"
)

func TestClientsEmpty(t *testing.T) {
	c, err := aetest.NewContext(nil)
	if err != nil {
		t.Fatal(err)
	}

	defer c.Close()

	room, err := getRoom(c, "testRoom")
	if err != nil {
		t.Error(err.Error())
	}

	clients, err := room.Clients(c)
	if err != nil {
		t.Error(err.Error())
	}

	if len(clients) != 0 {
		t.Error("clients should be empty")
	}
}

func TestClients(t *testing.T) {
	c, err := aetest.NewContext(nil)
	if err != nil {
		t.Fatal(err)
	}

	defer c.Close()

	room, err := getRoom(c, "testRoom")
	if err != nil {
		t.Error(err.Error())
	}

	_, err = room.AddClient(c, "testClient")
	if err != nil {
		t.Error(err.Error())
	}

	clients, err := room.Clients(c)
	if err != nil {
		t.Error(err.Error())
	}

	if len(clients) != 1 {
		t.Error("clients should not be empty")
	}
}

func TestClientExistsPositive(t *testing.T) {
	c, err := aetest.NewContext(nil)
	if err != nil {
		t.Fatal(err)
	}

	defer c.Close()

	room, err := getRoom(c, "testRoom")
	if err != nil {
		t.Error(err.Error())
	}

	_, err = room.AddClient(c, "testClient")
	if err != nil {
		t.Error(err.Error())
	}

	result, err := room.ClientExists(c, "testClient")
	if err != nil {
		t.Error(err.Error())
	}

	if result == false {
		t.Error("client should exist")
	}
}

func TestClientExistsNegative(t *testing.T) {
	c, err := aetest.NewContext(nil)
	if err != nil {
		t.Fatal(err)
	}

	defer c.Close()

	room, err := getRoom(c, "testRoom")
	if err != nil {
		t.Error(err.Error())
	}

	_, err = room.AddClient(c, "testClient")
	if err != nil {
		t.Error(err.Error())
	}

	result, err := room.ClientExists(c, "iDoNotExist")
	if err != nil {
		t.Error(err.Error())
	}

	if result == true {
		t.Error("client should not exist")
	}
}

func TestAddClientNew(t *testing.T) {
	c, err := aetest.NewContext(nil)
	if err != nil {
		t.Fatal(err)
	}

	defer c.Close()

	room, err := getRoom(c, "testRoom")
	if err != nil {
		t.Error(err.Error())
	}

	_, err = room.AddClient(c, "testClient")
	if err != nil {
		t.Error(err.Error())
	}
}

func TestAddClientExisting(t *testing.T) {
	c, err := aetest.NewContext(nil)
	if err != nil {
		t.Fatal(err)
	}

	defer c.Close()

	room, err := getRoom(c, "testRoom")
	if err != nil {
		t.Error(err.Error())
	}

	_, err = room.AddClient(c, "testClient")
	if err != nil {
		t.Error(err.Error())
	}

	// add the same client as above
	_, err = room.AddClient(c, "testClient")
	if err == nil {
		t.Error("adding same client should throw an error")
	}
}

func countRooms(c aetest.Context) (int, error) {
	q := datastore.NewQuery("Room").KeysOnly()
	var rooms []Room
	keys, err := q.GetAll(c, &rooms)
	if err != nil {
		return 0, err
	}
	return len(keys), nil
}
func TestGetRoomNew(t *testing.T) {
	c, err := aetest.NewContext(nil)
	if err != nil {
		t.Fatal(err)
	}

	defer c.Close()

	_, err = getRoom(c, "testRoom")
	if err != nil {
		t.Error(err.Error())
	}

	// give datastore time to catch up
	time.Sleep(1000 * time.Millisecond)

	cnt, err := countRooms(c)
	if err != nil {
		t.Error(err.Error())
	}
	if cnt != 1 {
		t.Error("there should only be 1 room")
	}
}

func TestGetRoomExisting(t *testing.T) {
	c, err := aetest.NewContext(nil)
	if err != nil {
		t.Fatal(err)
	}

	defer c.Close()

	_, err = getRoom(c, "testRoom")
	if err != nil {
		t.Error(err.Error())
	}

	// get the same room again
	_, err = getRoom(c, "testRoom")
	if err != nil {
		t.Error(err.Error())
	}

	cnt, err := countRooms(c)
	if err != nil {
		t.Error(err.Error())
	}
	if cnt != 1 {
		t.Error("there should only be 1 room")
	}
}

func TestRemoveClientExisting(t *testing.T) {
	c, err := aetest.NewContext(nil)
	if err != nil {
		t.Fatal(err)
	}

	defer c.Close()

	room, err := getRoom(c, "testRoom")
	if err != nil {
		t.Error(err.Error())
	}

	_, err = room.AddClient(c, "testClient")
	if err != nil {
		t.Error(err.Error())
	}

	clients, err := room.Clients(c)
	if err != nil {
		t.Error(err.Error())
	}
	if len(clients) != 1 {
		t.Error("there should be 1 client")
	}

	err = room.RemoveClient(c, "testClient")
	if err != nil {
		t.Error(err.Error())
	}

	clients, err = room.Clients(c)
	if err != nil {
		t.Error(err.Error())
	}
	if len(clients) != 0 {
		t.Error("there should be 0 clients")
	}
}
